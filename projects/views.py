from django.shortcuts import render, redirect
from projects.models import Project
from projects.forms import CreateProjectForm
from django.contrib.auth.decorators import login_required


@login_required
def all_projects(request):
    projects = Project.objects.filter(owner=request.user)

    context = {"projects": projects}

    return render(request, "pages/all_projects.html", context)


@login_required
def project_details(request, id):
    project = Project.objects.get(id=id)

    context = {"project": project}

    return render(request, "pages/project_details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(all_projects)

    else:
        form = CreateProjectForm()

    context = {
        "form": form,
    }
    return render(request, "pages/create_project.html", context)
