from django.urls import path
from projects.views import all_projects, project_details, create_project


urlpatterns = [
    path("", all_projects, name="list_projects"),
    path("<int:id>/", project_details, name="show_project"),
    path("create/", create_project, name="create_project"),
]
